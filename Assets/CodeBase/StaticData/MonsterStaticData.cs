using UnityEngine;

namespace CodeBase.StaticData
{
    [CreateAssetMenu(fileName = "MonsterData", menuName = "StaticData/Monster")]
    public class MonsterStaticData : ScriptableObject
    {
        public MonsterTypeId MonsterTypeId;

        public int MaxLoot;
        public int MinLoot;
    
        [Range(1,100)]
        public int HP;

        [Range(1f, 5f)] public float MoveSpeed;
        
        [Range(1f,30f)]
        public float Damage;

        [Range(0.5f,1f)]
        public float EffectiveDistance;
    
        [Range(0.5f,1f)]
        public float Cleavage;

        public GameObject prefab;
    }
}
