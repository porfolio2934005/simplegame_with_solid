namespace CodeBase
{
  public static class Constants
  {

    public static class FactoryConst
    {
      public const string HERO_PATH = "Hero/hero";
      public const string HUD_PATH = "Hud/Hud";
      public const string Loot = "Loot/Loot";
    }

    public const float Epsilon = 0.001f;
  }
}