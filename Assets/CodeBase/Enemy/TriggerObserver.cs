﻿ using System;
using UniRx;
using UnityEngine;

namespace CodeBase.Enemy
{
    [RequireComponent(typeof(Collider))]
    public class TriggerObserver : MonoBehaviour
    {
        public ReactiveCommand<Collider> TriggerEnter = new ReactiveCommand<Collider>();
        public ReactiveCommand<Collider> TriggerExit = new ReactiveCommand<Collider>();
        private void OnTriggerEnter(Collider other) => TriggerEnter?.Execute(other);

        private void OnTriggerExit(Collider other) => TriggerExit?.Execute(other);
    }
}