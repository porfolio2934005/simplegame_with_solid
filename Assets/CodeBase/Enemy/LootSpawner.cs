using System.Collections;
using System.Collections.Generic;
using CodeBase.Data;
using CodeBase.Enemy;
using CodeBase.Infrastructure.Factory;
using UniRx;
using UnityEngine;

public class LootSpawner : MonoBehaviour
{
    public EnemyDeath EnemyDeath;
    private IGameFactory _factory;
    private IRandomService _randomService;
    private int _lootMin;
    private int _lootMax;

    public void Construct(IGameFactory factory, IRandomService randomService)
    {
        _factory = factory;
        _randomService = randomService;
    }

    void Start()
    {
        //EnemyDeath.Happend.Subscribe(_ => SpawnLoot()).AddTo(this);
        EnemyDeath.Happend += SpawnLoot;
    }

    private void SpawnLoot()
    {
        GameObject loot = _factory.CreateLoot();
        loot.transform.position = transform.position;

        var lootItem = new Loot()
        {
            Value = _randomService.Next(_lootMin, _lootMax)
        };
        
        Debug.Log(lootItem.Value);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLoot(int min, int max)
    {
        _lootMin = min;
        _lootMax = max;
    }
}