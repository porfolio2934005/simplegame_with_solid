﻿using System;
using CodeBase.Logic;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CodeBase.Enemy
{
    public class EnemyAnimator : MonoBehaviour, IAnimationStateReader
    {
        private static readonly int Die = Animator.StringToHash("Die");
        private static readonly int Speed = Animator.StringToHash("Speed");
        private static readonly int IsMoving = Animator.StringToHash("IsMoving");
        private static readonly int Hit = Animator.StringToHash("Hit");
        private static readonly int Attack = Animator.StringToHash("Attack_1");

        private readonly int _idleStateHash = Animator.StringToHash("idle");
        private readonly int _attackStateHash = Animator.StringToHash("attack01");
        private readonly int _walkingStateHash = Animator.StringToHash("Move");
        private readonly int _deathStateHash = Animator.StringToHash("die");
        
        private Animator _animator;

        public AnimatorState State { get; private set; }

        //public event Action<AnimatorState> StateEntered;
        public ReactiveCommand<AnimatorState> StateEntered = new ReactiveCommand<AnimatorState>();
        public event Action<AnimatorState> StateExited; 

        private void Awake() => _animator = GetComponent<Animator>();

        public void PlayDeath() => _animator.SetTrigger(Die);
        public void PlayHit() => _animator.SetTrigger(Hit);

        public void Move(float speed)
        {
            _animator.SetBool(IsMoving, true);
            _animator.SetFloat(Speed, speed);
        }

        //Возврат в idle
        public void StopMoving() => _animator.SetBool(IsMoving, false);
        public void PlayAttack() => _animator.SetTrigger(Attack);


        public void EnteredState(int stateHash)
        {
            State = StateFor(stateHash);
            StateEntered.Execute(State);
            //StateEntered?.Invoke(State);
        }

        public void ExitedState(int stateHash) => StateExited?.Invoke(State);

        private AnimatorState StateFor(int stateHash)
        {
            AnimatorState state;

            //TODO здесь не подошёл оператор switch, так как требуеться константное значение, менять переменную на const от readonly не вижу смысла, так как в будущем значение может быть изменено
            if (stateHash == _idleStateHash)
                state = AnimatorState.Idle;
            else if (stateHash == _attackStateHash)
                state = AnimatorState.Attack;
            else if (stateHash == _walkingStateHash)
                state = AnimatorState.Walking;
            else if (stateHash == _deathStateHash)
                state = AnimatorState.Died;
            else
                state = AnimatorState.Unknown;

            return state;
        }
    }
}