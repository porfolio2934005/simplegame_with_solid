﻿using System;
using System.Collections;
using UniRx;
using UnityEngine;

namespace CodeBase.Enemy
{
    [RequireComponent(typeof(EnemyHealth), typeof(EnemyAnimator))]
    public class EnemyDeath : MonoBehaviour
    {
        [SerializeField] private EnemyHealth Health;
        [SerializeField] private EnemyAnimator Animator;

        public GameObject DeathFx;

        //public ReactiveCommand Happend = new ReactiveCommand();
        public Action Happend;

        private void Start() => Health.HealthChanged.Subscribe(_ => OnHealthChanged()).AddTo(this);

        private void OnDestroy() => Health.HealthChanged.Dispose();

        private void OnHealthChanged()
        {
            if (Health.Current <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            Health.HealthChanged.Dispose();
            Animator.PlayDeath();
            SpawnDeathSFX();
            StartCoroutine(DestroyTimer());

            Happend?.Invoke();
            //Happend?.Execute();
        }

        private void SpawnDeathSFX() => Instantiate(DeathFx, transform.position, Quaternion.identity);

        private IEnumerator DestroyTimer()
        {
            yield return new WaitForSeconds(3);
            Destroy(gameObject);
        }
    }
}