﻿using System;
using System.Linq;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

namespace CodeBase.Enemy
{
    public class AgentMoveToPlayer : Follow  
    {
        private const float MinimalDistance = 1;

        public NavMeshAgent Agent;

        private Transform _heroTransform;

        private IGameFactory _gameFactory;

        public void Construct(Transform heroTransform)
        {
            _heroTransform = heroTransform;
        }
        

        private void Update()
        { 
            if(CheckHeroOnNull() &&  HeroNotReached())
                Agent.destination = _heroTransform.position;
        }

        private bool CheckHeroOnNull() => _heroTransform != null;
        

        //Условие преследования пока расстояние больше минимальной дистанции

        private bool HeroNotReached() => 
            Vector3.Distance(Agent.transform.position, _heroTransform.position) >= MinimalDistance;
    }
}