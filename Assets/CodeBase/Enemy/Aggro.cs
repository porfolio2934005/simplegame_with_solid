﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using CodeBase.CameraLogic;
using UniRx;
using UnityEngine;

namespace CodeBase.Enemy
{
    public class Aggro : MonoBehaviour
    {
         [SerializeField] TriggerObserver TriggerObserver;
         [SerializeField] Follow Following;

         private IDisposable _aggroCorutine;
         [SerializeField] private float Cooldown;
         //TODO у сферы есть несколько точек соприкосновения. Она взаимодействует с разными коллайдерами
         //и тригеры enter и exit могут вызываться несколько раз подряд.
         //Это может вызвать некоторые проблемы с обработкой конечного результата.
         private bool _hasAggroTarget;

         private void Start()
        {
            TriggerObserver.TriggerEnter.Subscribe(_ => TrigerEnter()).AddTo(this);
            TriggerObserver.TriggerExit.Subscribe(_ => TriggerExit()).AddTo(this);

            SwitchFollowOff(); 
            
        }

        private void TrigerEnter()
        {
            if (!_hasAggroTarget)
            {
                _hasAggroTarget = true;
                StopAgroCoroutine();
                SwitchFollowOn();
            }
        }

        private  void TriggerExit()
        {
            if (_hasAggroTarget)
            {
                _hasAggroTarget = false;
                _aggroCorutine = Observable.FromCoroutine(SwitchFollowOffAfterCooldown)
                    .SelectMany(SwitchFollowOffAfterCooldown)
                    .Subscribe();
            }

            //StartCoroutine(SwitchFollowOffAfterCooldown());
        }

        private void StopAgroCoroutine()
        {
            if (_aggroCorutine != null)
            {
                _aggroCorutine.Dispose();
                _aggroCorutine = null;
            }
        }

        private IEnumerator SwitchFollowOffAfterCooldown()
        {
            yield return new WaitForSeconds(Cooldown);
            SwitchFollowOff();
        }

        private bool SwitchFollowOff() => Following.enabled = false;

        private bool SwitchFollowOn() =>  Following.enabled = true;
    }
}