﻿using System;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

namespace CodeBase.Enemy
{
   
    public class RotateToHero : Follow
    {
        [SerializeField] private float _speed;

        private Transform _heroTransform;
        private Vector3 _positionToLook;

        public void Construct(Transform heroTransform)
        {
            _heroTransform = heroTransform;
        }

        private void Update()
        {
            if (Initialized())
                RotateTowardHero();
        }

        private void RotateTowardHero()
        {
            UpdatePositionToLookAt();

            transform.rotation = SmoothedRotation(transform.rotation, _positionToLook);
        }

        //
        private Quaternion SmoothedRotation(Quaternion transformRotation, Vector3 positionToLook) => 
            Quaternion.Lerp(transformRotation, TargetRotation(positionToLook), SpeedFactor());

        private float SpeedFactor() => _speed * Time.deltaTime;

        private Quaternion TargetRotation(Vector3 positionToLook) => Quaternion.LookRotation(positionToLook);

        private void UpdatePositionToLookAt()
        {
            Vector3 positionDiff = _heroTransform.position - transform.position;
            _positionToLook = new Vector3(positionDiff.x, transform.position.y, positionDiff.z);
        }

        private bool Initialized() => _heroTransform != null;
    }
}