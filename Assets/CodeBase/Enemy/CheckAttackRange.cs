﻿using System;
using UniRx;
using UnityEngine;

namespace CodeBase.Enemy
{
    [RequireComponent(typeof(Attack))]
    public class CheckAttackRange : MonoBehaviour
    {
        public Attack Attack;
        public TriggerObserver TriggerObserver;

        private void Start()
        {
            TriggerObserver.TriggerEnter.Subscribe(_ => TriggerEnter())
                .AddTo(this);
            TriggerObserver.TriggerExit.Subscribe(_ => TriggerExit())
                .AddTo(this);

            Attack.DisableAttack();
        }

        //TODO проверка реактинвых команд
        public void TriggerEnter()
        {
            Attack.EnableAttack();
        }
        public void TriggerExit()
        {
            Attack.DisableAttack();
        }
    }
}