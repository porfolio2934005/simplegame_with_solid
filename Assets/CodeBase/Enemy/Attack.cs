using System.Linq;
using CodeBase.Enemy;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(EnemyAnimator))]
public class Attack : MonoBehaviour
{
    public float _clevage = 0.5f; //Радиус сферы
    public float _damage = 5f;
    public float _effectiveDistance = 0.5f;
    [SerializeField] private EnemyAnimator Animator;
    [SerializeField] float AttackCooldown = 3f;


    private Transform _heroTransform;
    private float _attackCooldown;
    private bool _isAttacking;
    private int _layerMask; //Битовая маска
    private Collider[] _hits = new Collider[1];  //Единица, так как пересекаеться только с игроком
    private bool _attackIsActive;

    private void Awake()
    {
        _layerMask = 1 << LayerMask.NameToLayer("Player");
    }

    public void Construct(Transform heroTransform) => 
        _heroTransform = heroTransform;

    private void Update()
    {
        //Обновление Cooldown
        UpdateCooldown();
        
        if(CanAttack())
            StartAttack();
    }

    private void UpdateCooldown()
    {
        if (!CooldownIsUp())
            _attackCooldown -= Time.deltaTime;
    }

    private void OnAttack()
    {
        if (Hit(out Collider hit))
        {
            PhysicsDebug.DrawDebug(StartPoint(), _clevage, 2f);
            hit.transform.GetComponent<IHealth>().TakeDamage(_damage);
        }
    }

    private bool Hit(out Collider hit)
    {
        var hitcount = Physics.OverlapSphereNonAlloc(this.StartPoint(), _clevage, _hits, _layerMask);

        hit = _hits.FirstOrDefault();

        return hitcount > 0;
    }

    private Vector3 StartPoint()
    {
        return new Vector3(transform.position.x, transform.position.y + 0.5f , transform.position.z) + transform.forward * _effectiveDistance;
    }

    private void OnAttackEnded()
    {
        _attackCooldown = AttackCooldown;
        _isAttacking = false; //Снятие индикатора в конце атаки
    }

    private bool CanAttack() => _attackIsActive && !_isAttacking && CooldownIsUp();

    private void StartAttack()
    {
        transform.LookAt(_heroTransform);
        Animator.PlayAttack();

        _isAttacking = true;
    }

    private bool CooldownIsUp()
    {
        return _attackCooldown <= 0f;
    }
    
    public void DisableAttack()
    {
        _attackIsActive = false;
    }

    public void EnableAttack()
    {
        _attackIsActive = true;
    }
}