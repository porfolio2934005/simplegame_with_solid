using System;
using CodeBase.Hero;
using CodeBase.Logic;
using UniRx;
using UnityEngine;

namespace CodeBase.UI
{
  public class ActorUI : MonoBehaviour
  {
    public HpBar HpBar;

    private IHealth _health;

    public void Construct(IHealth health)
    {
      _health = health;
      // _health.HealthChanged += UpdateHpBar;
      
      _health.HealthChanged
        .Subscribe(_ => UpdateHpBar())
        .AddTo(this);
    }

    private void Start()
    {
      IHealth health = GetComponent<IHealth>();
      
      if(health != null)
        Construct(health);
    }

    private void OnDestroy()
    {
      // _health.HealthChanged -= UpdateHpBar;
      //_health.HealthChanged?.Dispose();
    }

    private void UpdateHpBar()
    {
      HpBar.SetValue(_health.Current, _health.Max);
    }

  }
}