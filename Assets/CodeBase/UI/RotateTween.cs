using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI.FX
{
    public class RotateTween : MonoBehaviour
    {
        [SerializeField] private Vector3 _rotateTo;
        [SerializeField] private Vector3 _rotateFrom;
        [SerializeField] private float _duration;
        [SerializeField] private Ease _ease;
        [SerializeField] private LoopType _loopType;
        
        private Tween _tween;
        
        private void OnEnable() => Play();
        private void OnDisable() => Kill();


        private void Play()
        {
             _tween = transform
                .DOLocalRotate(_rotateTo, _duration)
                .From(_rotateFrom)
                .SetUpdate(true) //       Если true, то игнорирует остановку времени (time.scale)
                .SetLoops(-1, _loopType)
                .SetEase(_ease)
                .OnComplete(() => _tween = null);
        }                     
        
        private void Kill()     => _tween?.Kill();
    }
}