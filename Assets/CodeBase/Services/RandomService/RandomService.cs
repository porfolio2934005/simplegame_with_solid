﻿using UnityEngine;

class RandomService : IRandomService
{
    public int Next(int minValue, int maxValue) 
        => Random.Range(minValue, maxValue);
}