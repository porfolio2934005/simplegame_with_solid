﻿using System.ComponentModel;
using CodeBase.Infrastructure.AssertManagment;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using CodeBase.Infrastructure.Services.PersistentProgress;
using CodeBase.Infrastructure.Services.SaveLoad;
using CodeBase.Services.Input;
using CodeBase.StaticData;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class BootstrapState : IState
    {
        private const string INITIAL_SCENE_NAME = "Initial";
        private const string MAIN_SCENE_NAME = "Main";
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly AllServices _services;

        public BootstrapState(GameStateMachine stateMachine, SceneLoader sceneLoader, AllServices services)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
            _services = services;
            RegisterServices();
        }

        public void Enter()
        {
            _sceneLoader.Load(INITIAL_SCENE_NAME, onLoaded: EnterLoadLevel);
        }

        public void Exit()
        {
        
        }

        private void EnterLoadLevel()
        {
            _stateMachine.Enter<LoadProgressState>();
        }

        private void RegisterServices()
        {
            RegisterStaticData();
            _services.RegisterSingle<IInputService>(InputService());
            _services.RegisterSingle<IRandomService>(new RandomService());
            _services.RegisterSingle<IAsserts>(new Asserts());
            _services.RegisterSingle<IPersistentProgressService>(new PersistentProgressService());

            //TODO тут очень важен порядок инициализации. Если инициализировать до регистрации фабрики, SaveLoadService не инициализируется
            //Получаем из контейнера, но прежде надо зарегистрировать
            _services.RegisterSingle<IGameFactory>(new GameFactory(_services.Single<IAsserts>(), _services.Single<IStaticDataService>(), _services.Single<IRandomService>()));
            //Регистрация сервиса сохранения
            _services.RegisterSingle<ISaveLoadService>(new SaveLoadService(_services.Single<IPersistentProgressService>(), 
                _services.Single<IGameFactory>()));
        }

        private void RegisterStaticData()
        {
            IStaticDataService staticData = new StaticDataService();
            staticData.LoadMonsters();
            _services.RegisterSingle(staticData);
        }


        private static IInputService InputService()
        {
            if (Application.isEditor)
                return new StandaloneInputService();
            else
                return new MobileInputService();
        }
    }
    
    
}
