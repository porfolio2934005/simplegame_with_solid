﻿using CodeBase.Data;
using CodeBase.Infrastructure.Services.PersistentProgress;
using CodeBase.Infrastructure.Services.SaveLoad;

namespace CodeBase.Infrastructure.States
{
    public class LoadProgressState : IState
    {
        private readonly GameStateMachine _gameStateMachine;
        private readonly IPersistentProgressService _progressService;
        private readonly ISaveLoadService saveLoadService;

        public LoadProgressState(GameStateMachine gameStateMachine, IPersistentProgressService progressService, ISaveLoadService saveLoadService)
        {
            _gameStateMachine = gameStateMachine;
            _progressService = progressService;
            this.saveLoadService = saveLoadService;
        }

        public void Exit()  
        {
            
        }

        public void Enter()
        {
            //Загрузить или проинициализировать прогресс
            LoadProgressOrInitNew();
            _gameStateMachine.Enter<LoadLevelState, string>(_progressService.Progress.WorldData.PositionOnLevel.Level);   
        }

        private void LoadProgressOrInitNew()
        {
            _progressService.Progress = saveLoadService.LoadProgress() ?? NewProgress();
        }

        private PlayerProgress NewProgress()
        {
            var progress =  new PlayerProgress(initialLevel: "Main");
            progress.HeroState.MaxHP = 50;
            progress.HeroStats.Damage = 1f;
            progress.HeroStats.DamageRadius = 0.5f;
            progress.HeroState.ResetHP();
            
            return progress;
        }
        
        
    }
}