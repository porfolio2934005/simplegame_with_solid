﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CodeBase.Infrastructure.States
{
    public class SceneLoader
    {
        private readonly ICoroutineRunner _coroutineRunner;

        public SceneLoader(ICoroutineRunner coroutineRunner)
        {
            _coroutineRunner = coroutineRunner;
        }

        public void Load(string name, Action onLoaded = null) =>
            _coroutineRunner.StartCoroutine(LoadScene(name, onLoaded));

        public IEnumerator LoadScene(string nextScene, Action onLoaded = null)
        {
            //Нужно для того, чтобы не происходил постоянный циклический перезапуск
            if (SceneManager.GetActiveScene().name == nextScene)
            {
                onLoaded.Invoke();
                yield break;
            }

            AsyncOperation _waitNextScene = SceneManager.LoadSceneAsync(nextScene);

            while (!_waitNextScene.isDone)
                yield return null;

            onLoaded?.Invoke();
        }
    }
}