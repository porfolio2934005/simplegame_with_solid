﻿using CodeBase.Data;

namespace CodeBase.Infrastructure.Services.SaveLoad
{
    //TODO надо вспомнить, почему необходимо наследовать от IService
    public interface  ISaveLoadService : IService
    {
        void SaveProgress();
        PlayerProgress LoadProgress();
    }
}