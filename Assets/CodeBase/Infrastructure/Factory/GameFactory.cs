﻿using CodeBase.Infrastructure.AssertManagment;
using System.Collections.Generic;
using CodeBase.Enemy;
using CodeBase.Infrastructure.Services.PersistentProgress;
using CodeBase.StaticData;
using CodeBase.UI;
using UnityEngine;
using UnityEngine.AI;


namespace CodeBase.Infrastructure.Factory
{
    public class GameFactory : IGameFactory
    {

        private readonly IAsserts _assets;
        private readonly IStaticDataService _staticData;
        private readonly IRandomService _randomService;
        
        //Два свойства
        public List<ISavedProgressReader> ProgressReaders { get; } = new List<ISavedProgressReader>();
        public List<ISavedProgress> ProgressWriter { get; } = new List<ISavedProgress>();
        //TODO при вылите ошибки обратить внимание на модификатор сетера
        public GameObject HeroGameObject { get; private set; }

        public GameFactory(IAsserts assets, IStaticDataService staticData, IRandomService randomService)
        {
            _assets = assets;
            _staticData = staticData;
            _randomService = randomService;
        }

        public GameObject CreateHero(GameObject at)
        {
            HeroGameObject = InstantiateRegistered(Constants.FactoryConst.HERO_PATH, at.transform.position);
            return HeroGameObject;
        }

        public GameObject CreateHud() => 
            InstantiateRegistered(Constants.FactoryConst.HUD_PATH);

        //public event Action HeroCreated;

        public void CleanUp()
        {
            ProgressReaders.Clear();
            ProgressWriter.Clear();
        }


        public void Register(ISavedProgressReader progressReader)
        {
            //Если попал компонент записи - значит надо записать
            if(progressReader is ISavedProgress progressWriter)
                ProgressWriter.Add(progressWriter);
            
            ProgressReaders.Add(progressReader);
        }

        public GameObject CreateMonster(MonsterTypeId monsterTypeId, Transform parent)
        {
            MonsterStaticData monsterData = _staticData.ForMonster(monsterTypeId);
            GameObject monster = GameObject.Instantiate(monsterData.prefab, parent.position, 
                Quaternion.identity, parent);

            var health = monster.GetComponent<IHealth>();
            health.Current = monsterData.HP;
            health.Max = monsterData.HP;
            
            monster.GetComponent<ActorUI>().Construct(health);
            monster.GetComponent<AgentMoveToPlayer>().Construct(HeroGameObject.transform);
            monster.GetComponent<NavMeshAgent>().speed = monsterData.MoveSpeed;
            var lootSpawner = monster.GetComponentInChildren<LootSpawner>();
            lootSpawner.Construct(this, _randomService);
            lootSpawner.SetLoot(monsterData.MinLoot, monsterData.MaxLoot);
            var attack = monster.GetComponent<Attack>();
            attack.Construct(HeroGameObject.transform);
            attack._damage = monsterData.Damage;
            attack._clevage = monsterData.Cleavage;
            attack._clevage = monsterData.Cleavage;
            attack._effectiveDistance = monsterData.EffectiveDistance;
            attack.GetComponent<RotateToHero>()?.Construct(HeroGameObject.transform);
            return monster;
        }

        private GameObject InstantiateRegistered(string prefabPath, Vector3 position)
        {
            GameObject gameObject = _assets.Instantiate(prefabPath, at: position);
            RegisterProgressWatcher(gameObject);
            return gameObject;
        }

        private GameObject InstantiateRegistered(string prefabPath)
        {
            GameObject gameObject = _assets.Instantiate(prefabPath);
            RegisterProgressWatcher(gameObject);
            return gameObject;
        }

        public GameObject CreateLoot() => 
            InstantiateRegistered(Constants.FactoryConst.Loot);

        private void RegisterProgressWatcher(GameObject gameObject)
        {
            //Поиск компонентов и регистрация
            foreach (ISavedProgressReader progressReader in gameObject.GetComponentsInChildren<ISavedProgressReader>())
            {
                Register(progressReader);
            }
        }
    }
}