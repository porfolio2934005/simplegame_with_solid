﻿using System;
using UnityEngine;

namespace CodeBase.Infrastructure
{
    //Класс нужен для того, чтобы запускать игру с любого момента
    public class GameRunner : MonoBehaviour
    {
        public GameBootstrapper BootstrapperPrefab;
        
        private void Awake()
        {
            var bootstraper = FindObjectOfType<GameBootstrapper>();

            if (bootstraper == null)
                Instantiate(BootstrapperPrefab);

        }
    }
}