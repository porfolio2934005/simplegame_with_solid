using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;

public interface IHealth 
{
    
    // event Action HealthChanged;
    ReactiveCommand HealthChanged { get; }
    float Current { get; set; }
    float Max { get; set; }
    void TakeDamage(float damage);
}
