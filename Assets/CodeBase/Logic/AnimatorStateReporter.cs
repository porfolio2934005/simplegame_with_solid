using UnityEngine;

namespace CodeBase.Logic
{
  //StateMachineBehaviour - это способ Unity сообщать о том, что нам нужно
  public class AnimatorStateReporter : StateMachineBehaviour
  {
    private IAnimationStateReader _stateReader;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
      base.OnStateEnter(animator, stateInfo, layerIndex);
      FindReader(animator);

      _stateReader.EnteredState(stateInfo.shortNameHash);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
      base.OnStateExit(animator, stateInfo, layerIndex);
      FindReader(animator);

      _stateReader.ExitedState(stateInfo.shortNameHash);
    }

    //Эта функция необходима для того, чтобы связать StateMachineBehaviour с нашим кодом
    private void FindReader(Animator animator)
    {
      if (_stateReader != null)
        return;
      
      //Поиск по интерфейсу IAnimationStateReader
      _stateReader = animator.gameObject.GetComponent<IAnimationStateReader>();
    }
  }
}