﻿using System;
using System.Linq;
using System.Net.Mime;
using CodeBase.Logic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace CodeBase.Editor
{
    [CustomEditor(typeof(UniqueId))]
    public class UniqueIdEditor : UnityEditor.Editor
    {
        private void OnEnable()
        {
            var uniqueID = (UniqueId)target;

            if (string.IsNullOrEmpty(uniqueID.Id))
                Generete(uniqueID);
            else
            {
                UniqueId[] uniqueIds = FindObjectsOfType<UniqueId>();
                
                if(uniqueIds.Any(other => other != uniqueID && other.Id == uniqueID.Id))
                    Generete(uniqueID);
            }
        }

        private void Generete(UniqueId uniqueID)
        {
            uniqueID.Id = $"{uniqueID.gameObject.scene.name}_" +
                          $"{Guid.NewGuid().ToString()}";

            if (!Application.isPlaying)
            {
                EditorUtility.SetDirty(uniqueID);
                EditorSceneManager.MarkSceneDirty(uniqueID.gameObject.scene);
            }
        }
    }
}