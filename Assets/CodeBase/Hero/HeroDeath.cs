﻿using System;
using UniRx;
using UnityEngine;

namespace CodeBase.Hero
{
    [RequireComponent(typeof(HeroHealth))]
    public class HeroDeath : MonoBehaviour
    {
        public HeroHealth Health;
        public HeroMove Move;
        public HeroAttack Attack;
        public HeroAnimator Animator;
        public GameObject deathFX;

        private bool _isDead;

        private void Start() => 
            Health.HealthChanged.Subscribe(_ => HealthChanged()).AddTo(this);


        private void OnDestroy() => 
            Health.HealthChanged.Subscribe(_ => HealthChanged()).AddTo(this);

        private void HealthChanged()
        {
            if (!_isDead && Health.Current <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            _isDead = true;
            Move.enabled = false;
            Animator.PlayDeath();
            Attack.enabled = false;

            Instantiate(deathFX, transform.position, Quaternion.identity);
        }
    }
}