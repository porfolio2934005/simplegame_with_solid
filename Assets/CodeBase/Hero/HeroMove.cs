﻿using CodeBase.CameraLogic;
using CodeBase.Data;
using CodeBase.Infrastructure;
using CodeBase.Infrastructure.Services;
using CodeBase.Infrastructure.Services.PersistentProgress;
using CodeBase.Services.Input;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CodeBase.Hero
{
  [RequireComponent(typeof(CharacterController))]
  public class HeroMove : MonoBehaviour, ISavedProgress
  {
    [SerializeField] private CharacterController _characterController;
    [SerializeField] private float _movementSpeed;

    private IInputService _inputService;
    private Camera _camera;

    private void Awake()
    {
      _inputService = AllServices.Container.Single<IInputService>();
    }

    private void Update()
    {
      Vector3 movementVector = Vector3.zero;

      if (_inputService.Axis.sqrMagnitude > Constants.Epsilon)
      {
        movementVector = Camera.main.transform.TransformDirection(_inputService.Axis);
        movementVector.y = 0;
        movementVector.Normalize();

        transform.forward = movementVector;
      }

      movementVector += Physics.gravity;

      _characterController.Move(_movementSpeed * movementVector * Time.deltaTime);
    }


    public void UpdateProgress(PlayerProgress progress)
    {
      //Первый параметр - текущий уровень, второй параметр - позиция на уровне
      progress.WorldData.PositionOnLevel =
        new PositionOnLevel(CurrentLevel(), transform.position.AsVectorData());
    }

    public void LoadProgress(PlayerProgress progress)
    {
      if (CurrentLevel() == progress.WorldData.PositionOnLevel.Level)
      {
        //Загрузка позиции
        Vector3Data savedPosition = progress.WorldData.PositionOnLevel.Position;
        if (savedPosition != null)  //Проверка не являеться ли блок данных с позицией пустышкой
        {
          Warp(to: savedPosition);
        }
      }

    }

    private void Warp(Vector3Data to)
    {
      //Временное отключение контроллера производиться для избегания лишних багов,
      //Так как может произойти ситуация в которой физика будет некорректно обработана и контроллер может застрять
      _characterController.enabled = false;
        //Расширение для преобразования в Unity вектор
      transform.position = to.AsUnityVector().AddY(_characterController.height * 2);  //Расширение добавлено чтобы персонаж инциализировался немножко выше
      _characterController.enabled = true;
    }

    private static string CurrentLevel()
    {
      return SceneManager.GetActiveScene().name;
    }
  }
}